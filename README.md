# Listings Administration Application
## Frontend 

## Backend
Dotnet core

## Docker
- In order to persist what we do in docker, we need to create a volume for our containers to use.

```
docker volume create [volume_name]
```

*** the value for volume_name should be the same used for the volumes in our docker-compose.yml file

- Starting our services is as simple as `docker-compose up -d` from the root directory of our project (same directory as our docker-compose.yml file)

- Caveats: if the port is currently being used by another docker container instance, or another application you'll need to stop that container/application
- List running containers: `docker ps [-a]` adding the '-a' flag lists stopped conainers also


## Todo
- Move configuration to config server
- configure runtime endpoint changes for front-end
