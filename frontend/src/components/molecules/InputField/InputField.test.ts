import DependencyFactory from '@/lib/DependencyFactory';

const { log } = DependencyFactory.logger;
it('InputField renders without crashing', () => {
  const fieldWrapperSelector = 'div.field.is-horizontal';
  const fieldLabelWrapperSelector = 'div.field-label.is-normal';
  const fieldBodyWrapperSelector = 'div.field-body';

  const div = document.createElement('div');


  const fieldWrapper = div.querySelector(fieldWrapperSelector);
  const fieldLabelWrapper = div.querySelector(fieldLabelWrapperSelector);
  const fieldBodWrapper = div.querySelector(fieldBodyWrapperSelector);

  expect(fieldWrapper).toBeTruthy();
  expect(fieldLabelWrapper).toBeTruthy();
  expect(fieldBodWrapper).toBeTruthy();

});
