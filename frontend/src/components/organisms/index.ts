import AppLoginModal from './Login/AppLoginModal.vue';
import AppSignupModal from './Signup/AppSignupModal.vue';

export default { AppLoginModal, AppSignupModal };