import { InputType } from '@/lib/Forms';
import { typeAttr } from '../../../lib/CssUtils';
import DependencyFactory from '../../../lib/DependencyFactory';
import { Logger } from '../../../lib/types/types';

const { log }: Logger = DependencyFactory.logger;
describe('input tests: ', () => {
  it('TextInput renders default text input', () => {
    const expectedSelector = 'input.input[type="text"]';
    const div = document.createElement('div');

  });

  it('TextInput renders password input', () => {
    const expectedSelector = 'input.input[type="password"]';
    const attrs = typeAttr(InputType.PASSWORD);
    const props = { attrs };
    const div = document.createElement('div');

  });

  it('TextInput renders email input', () => {
    const expectedSelector = 'input.input[type="email"]';
    const attrs = typeAttr(InputType.EMAIL);
    const props = { attrs };
    const div = document.createElement('div');

  });

  it('TextInput renders telephone input', () => {
    const expectedSelector = 'input.input[type="tel"]';
    const attrs = typeAttr(InputType.TEL);
    const props = { attrs };

  });

});
