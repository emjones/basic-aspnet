const enumToMap = (enumeration: any) => {
  const map: any = {};
  for (const key in enumeration) {
    if (typeof enumeration[key] === 'number' || typeof enumeration[key] === 'string') {
      map[key] = enumeration[key];
    }
  }
  return map;
};
