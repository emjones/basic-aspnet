import { AuthClientConfig } from '@/lib/types/auth';
import { PhoneNumberType } from '@/lib/User/Forms';
import axios from 'axios';

// const { log } = DependencyFactory.logger;
const log = console.log;
/**
 * User Endpoints
 */
const CREATE_USER = '/api/users';
const USER_PHOTO = '/api/users/:id/photo';
const UPDATE_USER: string = '/api/users';

/**
 * Phone Number Endpoints
 */

const PHONE_NUMBER = '/api/phone-number';

/**
 * Invoice Endpoints
 */
const CREATE_INVOICE = '/api/invoices';

/**
 * Telemetry endpoints
 */
const TELEMETRY_FORM_LOG = '/api/telemetry/form';
const TELEMETRY_EVENT_LOG = '/api/telemetry/event';

const asyncRequest: (requestType: any) => any = (requestType: any) => {
  return async (path: string, data: any) => {
    try {
      return requestType(path, data);
    } catch (e) {
      log(e);
    }
  };
};

export const post: (path: string, data: any) => any
  = async (path, data) => asyncRequest(axios.post)(path, data);

export const patch: (path: string, data: any) => any
  = async (path, data) => asyncRequest(axios.patch)(path, data);

interface CreateUserResponse {
  id: string;
}

export interface UserFormData {
  email: string;
  password: string;
  photo?: string;
  firstName: string;
  lastName: string;
}

interface InvoiceRequest {
  userId: string;
  invoiceId: string;
  listingId?: string;
}

interface InvoiceItem {
  name: string;
  amount: string;
}

interface Invoice {
  invoiceNumber: string;
  invoiceAmount: number;
  invoiceItems: InvoiceItem[];
}

interface InvoiceResponse {
  invoices: Invoice[];
}

interface OrderListingRequest {
  listingId: string;
  startDate: string;
  endDate: string;
}

interface PhoneNumberData {
  userId: string;
  type: PhoneNumberType;
  number: string;
}

interface CreatePhoneNumberRequest {
  data: PhoneNumberData;
}

interface CreatePhotoResponse {
  id: string;
}

/**
 * user api
 */
export const createUser: (userForm: UserFormData) => Promise<CreateUserResponse>
  = (userForm: UserFormData) => post(CREATE_USER, userForm);
export const update: (userForm: UserFormData) => Promise<CreateUserResponse>
  = (userForm: UserFormData) => patch(UPDATE_USER, userForm);
export const uploadPhoto: (file: File)
  => Promise<CreatePhotoResponse> = (file: File) => post(USER_PHOTO, file);

/**
 * Phone number api
 */
export const createPhoneNumber: (phoneNumberForm: PhoneNumberData) => Promise<CreatePhoneNumberRequest>
  = (phoneNumberForm: PhoneNumberData) => post(PHONE_NUMBER, phoneNumberForm);

interface FormTelemetryResponse {
  status: string;
}

interface EventTelemetryResponse {
  status: string;
}

/**
 * Telemetry api
 */
export const logFormTelemetry: (formTelemetryData: FormTelemetryData) => Promise<FormTelemetryResponse>
  = (formTelemetryData: FormTelemetryData) => post(TELEMETRY_FORM_LOG, formTelemetryData);

export const logEventTelemetry: (eventTelemetryData: EventTelemetryData) => Promise<EventTelemetryResponse>
  = (eventTelemetryData: EventTelemetryData) => post(TELEMETRY_EVENT_LOG, eventTelemetryData);

/**
 * Auth
 */

export const authClientCredentials: () => Promise<AuthClientConfig> = () => {
  return new Promise(resolve => {
    return {
      domain: 'anendeavor.auth0.com',
      clientID: 'fxGf76p2A5KtsWPJh4GvJI8cpifTkHqq',
      redirectUri: `${process.env.VUE_APP_BASE_URL}/login/true`,
      responseType: 'token id_token',
      scope: 'openid',
    };
  });
};
