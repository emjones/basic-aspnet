import '@babel/polyfill';
import { isEmail, isLength } from 'validator';

export const SPECIAL_CHARS = /^[#$^+=!*()@%&]/;
export const NUMBERS_AND_LETTERS = /^[a-zA-Z 0-9]*$/;
export const PASSWORD_VALIDATOR = /^(?=.*[a-zA-Z 0-9 #$^+=!*()@%&])/;

export enum Validators {
  MAX_NAME_LENGTH = 100,
  MIN_NAME_LENGTH = 2,
  MIN_PASSWORD_LENGTH = 8,
}

export const getInvalidCaracters = (charString: string) => charString
  .split('')
  .filter((char: string) => !PASSWORD_VALIDATOR.test(char))
  .join('');

export const validName =
  (val: string) => isLength(val, {
    min: Validators.MIN_NAME_LENGTH,
    max: Validators.MAX_NAME_LENGTH,
  });

export const validFormValue = (validator: (val: string) => boolean) => {
  return (value: string, wasTouched: boolean) => validator(value) && wasTouched;
};
export const validPassword: (val: string) => boolean =
  (val: string) => isLength(val, { min: Validators.MIN_PASSWORD_LENGTH }) &&
    getInvalidCaracters(val).length === 0;

export const validFormEmail: (value: string, wasTouched: boolean) => boolean
  = validFormValue(isEmail);
export const validFormName: (value: string, wasTouched: boolean) => boolean
  = validFormValue(validName);
export const validFormPassword: (value: string, wasTouched: boolean)
  => boolean = validFormValue(validPassword);

export enum InputType {
  FILE = 'file',
  TEXT = 'text',
  PASSWORD = 'password',
  EMAIL = 'email',
  TEL = 'tel',
}

export enum InputStyle {
  ROUNDED = 'is-rounded',
  READONLY = 'is-readonly',
  DISABLED = 'is-disabled',
  HAS_ICONS_L = 'has-icons-left',
  HAS_ICONS_R = 'has-icons-right',
}

export interface InputProps {
  classes?: InputStyle[];
  readOnly?: any;
  disabled?: any;
  type?: InputType;
  value?: string;
  onChange?: (val: any) => void;
  isValid?: boolean;
}
