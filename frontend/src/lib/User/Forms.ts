import AuthService from '@/lib/auth-service';
import DependencyFactory from '@/lib/DependencyFactory';
import { UserState } from '@/lib/types/user';
import { createUser } from '../api';

const { log } = DependencyFactory.logger;

export enum PhoneNumberType {
  MOBILE = 'Mobile',
  HOME = 'Home',
  OFFICE = 'Office',
}

export class UserProfile {
  public static state: UserState = {
    email: '',
    firstName: '',
    lastName: '',
    password: '',
    photo: '',
    formElementsInteractedWith: [],
    auth: {
      authClientConfig: {
        domain: 'anendeavor.auth0.com',
        clientID: 'fxGf76p2A5KtsWPJh4GvJI8cpifTkHqq',
        redirectUri: `${process.env.VUE_APP_BASE_URL}/login/success`,
        responseType: 'token id_token',
        scope: 'openid',
      },
      authClient: new AuthService(),
      auth: AuthService.state,
    },
  };

  public static submit: (userForm: UserState) => void = async (userForm: UserState) => {
    try {
      const { email, firstName, lastName, photo, password } = userForm;
      const response = await createUser({ email, firstName, lastName, photo, password });
    } catch (e) {
      log(e);
    }
  }
}
