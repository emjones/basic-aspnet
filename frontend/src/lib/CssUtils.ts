import {InputStyle, InputType} from '@/lib/Forms';

export const classPrefixer = (prefix: string, className: string) => `${prefix}-${className}`;
export const bfixHas = (className?: string) => className ? classPrefixer('has', className) : '';
export const bfix = (className?: string) => className ? classPrefixer('is', className) : '';
export const classesToString = (classes: Array<InputStyle | string>) => classes.join(' ');

export enum GeneralStyleEnum {
  HOVER = 'hovered',
  FOCUS = 'focused',
  LOADING = 'loading',
  DISABLED = 'disabled',
}

export enum ColorEnum {
  PRIMARY = 'is-primary',
  SUCCESS = 'is-success',
  INFO = 'is-info',
  WARNING = 'is-warning',
  DANGER = 'is-danger',
}

export enum SizeEnum {
  SMOLL = 'small',
  MEDIUM = 'medium',
  LARGE = 'large',
}

// input helpers
export const typeAttr = (type: InputType) => ({ type });
