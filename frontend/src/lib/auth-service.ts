import { authClientCredentials } from '@/lib/api';
import { AuthClientConfig, AuthCredentials, AuthIdentityState } from '@/lib/types/auth';
import router from '@/router';
// src/Auth/AuthService.js
import auth0 from 'auth0-js';
import EventEmitter from 'eventemitter3';
import Storage from './storage';

export default class AuthService {
  public static state: AuthIdentityState = {
    accessToken: '',
    idToken: '',
    expiresAt: '',
  };

  private auth0?: auth0.WebAuth;
  public authNotifier = new EventEmitter();
  public login = () => this.auth0 ? this.auth0.authorize() : async () => {
    const clientCookie: AuthClientConfig = await authClientCredentials();
    this.auth0 = new auth0.WebAuth(clientCookie);
    this.auth0.authorize();
    console.log('appears to be offline');
  };

  // ...
  public handleAuthentication = () => {
    this.auth0 ? this.auth0.parseHash((err, authResult: auth0.Auth0DecodedHash | null) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        this.setSession(authResult);
        router.replace('home');
      } else if (err) {
        router.replace('home');
        console.log(err);
      }
    }) : () => {
    };
  }

  public renewSession = () => {
    this.auth0 ? this.auth0.checkSession({}, (err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        this.setSession(authResult);
      } else if (err) {
        this.logout();
        console.log(err);
      }
    }) : null;
  }

  public logout() {
    // Clear access token and ID token from local storage
    this.authNotifier.emit('authChange', false);

    Storage.setLoggedIn(false);
    Storage.removeAuthInfo();
    // navigate to the home route
    router.replace('home');
  }

  public setClient: (clientCookie: AuthClientConfig) => void = (clientCookie: AuthClientConfig) => {
    this.auth0 = new auth0.WebAuth(clientCookie);
  }

  public getAuthenticatedFlag() {
    return localStorage.getItem('loggedIn');
  }

  public isAuthenticated() {
    const { expiresIn } = Storage.getAuthInfo();
    return expiresIn
      && new Date().getTime() < expiresIn
      && this.getAuthenticatedFlag() === 'true';
  }

  private setSession(authResult: AuthCredentials) {
    Storage.setAuthInfo(authResult);
    this.authNotifier.emit('authChange', { authenticated: true, ...authResult });
  }

}
