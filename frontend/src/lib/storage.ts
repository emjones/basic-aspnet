import { AuthCredentials } from '@/lib/types/auth';

enum StorageKeys {
  LOGGED_IN = 'loggedIn',
  AUTHENTICATION = 'authentication',
}

export default class Storage {

  public static setLoggedIn(isLoggedIn: boolean) {
    localStorage.setItem(StorageKeys.LOGGED_IN, '' + isLoggedIn);
  }

  public static getLoggedIn(): boolean {
    return localStorage.getItem(StorageKeys.LOGGED_IN) === 'true';
  }

  public static setAuthInfo(authInfo: AuthCredentials | null) {
    localStorage.setItem(StorageKeys.AUTHENTICATION, JSON.stringify(authInfo));
  }

  public static removeAuthInfo() {
    localStorage.removeItem(StorageKeys.AUTHENTICATION);
  }

  public static getAuthInfo(): AuthCredentials {
    const authInfoString: string | null = localStorage.getItem(StorageKeys.AUTHENTICATION);
    return authInfoString ? JSON.parse(authInfoString) : null;
  }
}
