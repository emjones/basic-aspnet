import faker from 'faker';
import { bfix, bfixHas, classesToString } from './CssUtils';

describe('CssUtils', () => {
  it('properly use bulma prefix', () => {
    for (let i = 0; i < 100; i++) {
      const fakeString: string = faker.name.firstName();
      expect(bfix(fakeString)).toEqual(`is-${fakeString}`);
    }
  });

  it('propertly format class array to string', () => {
    for (let i = 0; i < 100; i++) {
      const s1: string = faker.name.firstName().toLowerCase();
      const s2: string = faker.name.firstName().toLowerCase();
      const s3: string = faker.name.firstName().toLowerCase();
      const s4: string = faker.name.firstName().toLowerCase();
      const validClassString = `${s1} ${s2} ${s3} ${s4}`;
      const testClasses = [s1, s2, s3, s4];
      expect(classesToString(testClasses)).toEqual(validClassString);
    }
  });


  it('test bfix has', () => {
    for (let i = 0; i < 100; i++) {
      const val: string = faker.name.firstName().toLowerCase();
      expect(bfixHas(val)).toEqual(`has-${val}`);
    }
  });
});
