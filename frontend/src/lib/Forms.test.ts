import { isLength } from 'validator';
import { getInvalidCaracters, NUMBERS_AND_LETTERS, SPECIAL_CHARS } from './Forms';

describe('Test forms library', () => {
  it('test special character validation', () => {
    expect(SPECIAL_CHARS.test('#')).toBeTruthy();
    expect(SPECIAL_CHARS.test('~')).toBeFalsy();
    // multiple, one invalid
    expect(SPECIAL_CHARS.test('~#')).toBeFalsy();
    // multiple valid
    expect(SPECIAL_CHARS.test('!#')).toBeTruthy();
  });

  it('test normal character validation', () => {
    expect(NUMBERS_AND_LETTERS.test('A0a1')).toBeTruthy();
    expect(NUMBERS_AND_LETTERS.test('~')).toBeFalsy();
    // multiple, one invalid
    expect(NUMBERS_AND_LETTERS.test('1#')).toBeFalsy();
    expect(NUMBERS_AND_LETTERS.test('a#')).toBeFalsy();
  });

  it('password validator', () => {
    expect(getInvalidCaracters('~')).toBeTruthy();
    expect(getInvalidCaracters('!')).toBeFalsy();
    expect(getInvalidCaracters('a1234~')).toBeTruthy();
    expect(getInvalidCaracters('12#a10')).toBeFalsy();
  });

  it('test validator framework', () => {
    expect(isLength('123456', { min: 1, max: 5 })).toBeFalsy();
  });
});
