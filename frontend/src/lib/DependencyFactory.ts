import { logEventTelemetry, logFormTelemetry } from '@/lib/api';
import { Logger } from './types/types';

export default class DependencyFactory {
  public static logger: Logger = {
    log: console.log,
    logFormData: logFormTelemetry,
    logUserInteraction: logEventTelemetry,
  };
}
