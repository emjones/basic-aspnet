export interface Logger {
  log: (message?: any, ...optionalParams: any[]) => void;
  logFormData: (formDataTelemetry: FormTelemetryData) => void;
  logUserInteraction: (eventTelemetry: EventTelemetryData) => void;
}



