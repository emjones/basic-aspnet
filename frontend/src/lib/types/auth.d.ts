/**
 * START AUTH TYPES
 */
import AuthService from '@/lib/auth-service';

export interface AuthClientConfig extends auth0.AuthOptions {
}

export interface AuthCredentials extends auth0.Auth0DecodedHash {

}

export interface AuthIdentityState {
  accessToken?: string;
  idToken?: string;
  expiresAt?: string;
}

export interface AuthState {
  authClientConfig: AuthClientConfig;
  authClient: AuthService;
  auth: AuthIdentityState;
}
