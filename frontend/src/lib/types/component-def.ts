export enum BulmaSize {
  SMALL = 'is-small',
  MEDIUM = 'is-medium',
  LARGE = 'is-large',
}

export enum FaIcon {
  SIGN_IN = 'fas fa-sign-in-alt',
  SIGN_UP = 'fas fa-user-plus',
  GITHUB = 'fab fa-github',
}
