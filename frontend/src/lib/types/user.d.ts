import { AuthState } from '@/lib/types/auth';

export interface UserState extends HasFormElements {
  email: string;
  firstName: string;
  lastName: string;
  password: string;
  photo?: string;
  auth: AuthState;
}
