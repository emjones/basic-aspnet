interface FormTelemetryData {
  userId: string;
  formData: any;
  timestamp: string;
}

interface EventTelemetryData {
  userId: string;
  event: Event;
  timestamp: string;
}
