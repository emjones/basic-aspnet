export enum Module {
  USER_ADMIN = 'userAdmin',
}

const stores = {
  userAdmin: {
    create: 'createUser',
    update: 'updateUser',
    identity: 'identity',
    initService: 'initService',
    handleAuth: 'handleAuth',
  },
  logging: {
    userFrom: 'userForm',
    domEvent: 'domEvent',
  },
};

export default { stores };
