import { userAdmin } from '@/store/userAdmin';
// index.ts
import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';

Vue.use(Vuex);

export interface RootState {
  version: string;
}

const store: StoreOptions<RootState> = {
  state: {
    version: '1.0.0',

  },
  modules: {
    userAdmin,
  },
};

export default new Vuex.Store<RootState>(store);
