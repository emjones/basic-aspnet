import { UserState } from '@/lib/types/user';
import { RootState } from '@/store';
import { GetterTree } from 'vuex';

export const getters: GetterTree<UserState, RootState> = {
  user(state): UserState {
    return state;
  },
};
