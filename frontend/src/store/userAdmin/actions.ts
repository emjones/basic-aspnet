import { UserState } from '@/lib/types/user';
import { RootState } from '@/store';
import { ActionContext, ActionTree } from 'vuex';

export const actions: ActionTree<UserState, RootState> = {
  identity({ state, commit }: ActionContext<UserState, RootState>) {
    const { auth } = state;
    console.log(auth.authClient);
    auth.authClient.login();
  },
  init({ state, commit }: ActionContext<UserState, RootState>) {
    const { auth } = state;
    const { authClientConfig } = auth;
    auth.authClient.setClient(authClientConfig);
  },
};
