import { UserState } from '@/lib/types/user';
import { UserProfile } from '@/lib/User/Forms';
import { RootState } from '@/store';
import { Module } from 'vuex';
import { actions } from './actions';
import { getters } from './getters';
import { mutations } from './mutations';

export const state: UserState = UserProfile.state;

const namespaced: boolean = true;
export const userAdmin: Module<UserState, RootState> = {
  namespaced, state, getters, actions, mutations,
};
