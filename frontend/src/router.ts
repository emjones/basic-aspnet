import Signup from '@/components/organisms/Signup/AppSignupModal.vue';
import Home from '@/components/pages/Home/Home.vue';

import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/signup/:success',
      name: 'signup',
      component: Signup,
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ '@/components/pages/About/About.vue'),
    },
    {
      path: '/login/:success',
      name: 'login',
      component: () => import(/* webpackChunkName: "login" */ '@/components/organisms/Login/AppLoginModal.vue'),
    },
  ],
});
