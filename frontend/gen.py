import argparse
import jinja2
import os.path
from enum import Enum

templateLoader = jinja2.FileSystemLoader(searchpath="./")
templateEnv = jinja2.Environment(loader=templateLoader)


# dependency 1


class ComponentType(Enum):
    CLASS = 'class'  # default
    FUNCTIONAL = 'fun'
    HOC = 'hoc'
    ATOM = 'a'  # default
    MOLECULE = 'm'
    ORGANISM = 'o'
    TEMPLATE = 't'
    PAGE = 'p'


# dependency 2


def define_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('component_name', default='Component', nargs='?')

    parser.add_argument('--class', '-c', dest='component_type',
                        action='store_const', const=ComponentType.CLASS)
    parser.add_argument('--functional', '--func', '-f', dest='component_type',
                        action='store_const', const=ComponentType.FUNCTIONAL)
    parser.add_argument('--hoc', dest='component_type',
                        action='store_const', const=ComponentType.HOC)

    parser.add_argument('--atom', '-a', dest='chem_component_type',
                        action='store_const', const=ComponentType.ATOM)
    parser.add_argument('--molecule', '-m', dest='chem_component_type',
                        action='store_const', const=ComponentType.MOLECULE)
    parser.add_argument('--organism', '-o', dest='chem_component_type',
                        action='store_const', const=ComponentType.ORGANISM)
    parser.add_argument('--template', '-t', dest='chem_component_type',
                        action='store_const', const=ComponentType.TEMPLATE)
    parser.add_argument('--page', '-p', dest='chem_component_type',
                        action='store_const', const=ComponentType.PAGE)
    (component_name, component_type, chem_component_type) = vars(
        parser.parse_args()).values()

    return (component_name, chem_component_type, component_type)


def write(directory, extension, component_name, content):
    filename = "%s/%s.%s" % (directory, component_name, extension)
    component_exists = os.path.isfile(filename)
    dir_exists = os.path.isdir(directory)
    if component_exists:
        raise Exception('Component Exists')
    if dir_exists:
        text_file = open(filename, "w")
        text_file.write(content)
        text_file.close()
    else:
        os.makedirs(os.path.dirname(filename))
        text_file = open(filename, "w")
        text_file.write(content)
        text_file.close()


def get_dir(atomic_component_type, component_name):
    base_dir = './src/components'
    dir_name = 'atoms'
    dir_name = 'molecules' if atomic_component_type == ComponentType.MOLECULE else dir_name
    dir_name = 'organisms' if atomic_component_type == ComponentType.ORGANISM else dir_name
    dir_name = 'templates' if atomic_component_type == ComponentType.TEMPLATE else dir_name
    dir_name = 'pages' if atomic_component_type == ComponentType.PAGE else dir_name
    return "%s/%s/%s" % (base_dir, dir_name, component_name)


(component_name, chem_component_type, component_type) = define_args()

dir = get_dir(chem_component_type, component_name)

component_template_name = 'ftemplate.j2' if component_type == ComponentType.FUNCTIONAL else 'template.j2'
component_template_name = 'hoctemplate.j2' if component_type == ComponentType.HOC else component_template_name

component_template = templateEnv.get_template(component_template_name)
print(chem_component_type)
print(component_template_name)
test_template = templateEnv.get_template('test-template.test.j2')

component_content = component_template.render(component_name=component_name)
test_content = test_template.render(component_name=component_name)

write(dir, 'tsx', component_name, component_content)
write(dir, 'test.tsx', component_name, test_content)
