using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Tuts
{
    public class Linq
    {
        static void Main(string[] args)
        {
            string path = @"../";
            // ShowLargeFilesInProject(path);
            ShowLargeFilesWithLinq(path);
        }

        private static void ShowLargeFilesInProject(string path)
        {
            DirectoryInfo directory = new DirectoryInfo(path);
            FileInfo[] files = directory.GetFiles();
            foreach (FileInfo file in files)
            {
                Console.WriteLine($"{file.Name}:{file.Length}");
            }
        }

        private static void ShowLargeFilesWithLinq(string path)
        {
            var query = from file in new DirectoryInfo(path).GetFiles()
            orderby file.Length descending
            select file;

            foreach (var file in query.Take(5))
            {
                Console.WriteLine($"{file.Name}:{file.Length}");

            }
        }

    }
}