# General Skills

## Programming

### General

- [Object Oriented C#](https://app.pluralsight.com/library/courses/object-oriented-programming-fundamentals-csharp/table-of-contents) - 1
  - You want to leave this course with a strong comfort of interfaces
- [Improving Basics Code best Practices](https://app.pluralsight.com/library/courses/csharp-best-practices-improving-basics/table-of-contents) - 2
- [Working with Nulls](https://app.pluralsight.com/library/courses/csharp-nulls-working/table-of-contents) - 2
- [Best Practices: Collections and Generics](https://app.pluralsight.com/library/courses/csharp-best-practices-collections-generics/table-of-contents) - 2
- [Linq](https://app.pluralsight.com/library/courses/linq-fundamentals-csharp-6/table-of-contents) - Linux or Windows - 2

### .NET Core API - Basics - 1 - Work in Windows

This will introduce some of the higher-level programming architecture that will be useful in understanding MVC

- Understanding separation of concerns/responsibilities
- Composition vs. Inheritence
- Interfaces

- [Pluralsight - ASP.NET Core API Building - Beginner](https://app.pluralsight.com/library/courses/asp-dotnet-core-api-building-first/table-of-contents) - 1

- Model
- View
- Controller

Answer:
What is ASP.NET?
What is .NET Core?

Topics Covered

- Dependency Injection
  - Overview with em
  - How to within ASP.NET
  - What's the point?
- Configuration

  - What is configuration used for?

* [Pluralsight - Testing .NET Core Code with xUnit.net](https://app.pluralsight.com/library/courses/dotnet-core-testing-code-xunit-dotnet-getting-started/table-of-contents) - 1

  - What is a unit test?

## Architecture

### Client-Server

#### Backend

##### DBAL - 1

---

- What is it?
- Is this a client or a server layer? Why?
- What is an ORM?
- What are migrations?

##### Business Logic - 1

---

- What is it?
- Is this a client or a server layer? Why?

#### Event Bus

Interfaces - includes brief dependency injection module at the end - pluralsight
Writing clean and testable C# code - pluralsight

## High-Level Concepts Resources

- [Client-Server](https://www.youtube.com/watch?v=SwLdKeC8scE) - 1
- [Dependency Injection](https://www.youtube.com/watch?v=Eqi-hYX50MI) - 1
- [Rest API](https://www.youtube.com/watch?v=qVTAB8Z2VmA) - 1
- [MVC](https://www.youtube.com/watch?v=pCvZtjoRq1I) - 1
- [Git](https://www.youtube.com/watch?v=SWYqp7iY_Tc) - 2

When you Feel like a Baller:

- [State Machines](https://www.smashingmagazine.com/2018/01/rise-state-machines/) - 3
- [CQRS](https://martinfowler.com/bliki/CQRS.html) - 3
- Agile - 3

Podcast Episodes:

- [Developing For Voice](https://thewebplatformpodcast.com/179-developing-for-voice)
- [Verge](https://www.theverge.com/podcasts)

## Project

- Architecture
- ## Listings Endpoints
