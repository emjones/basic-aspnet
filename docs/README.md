# Emma and Em's Learning and Long Term Project

## Dependencies - Windows Env

- Visual Studio
  - ASP.NET and Web Development
  - .NET Core
- VS Code
  - Extensions: 
    - Gitlens
    - Django
    - ES7 React/Redux/GraphQL...
    - HTML CSS Support
    - Jinja
    - Jupyter
    - MagicPython
    - markdownlint
    - Python
    - Python Extensions Pack
    - Python Preview
    - Quokka.js
    - TSLint
    - Typescript Importer
    - Visual Studio Intellicode - Preview
- [git](https://git-scm.com/download/win)
- [Postman](https://www.getpostman.com/)

- [Python](https://www.python.org/ftp/python/3.7.1/python-3.7.1-amd64.exe)
  - Make sure to click the option that installs it to your path
  - After install, select the option that changes your path max length



Optional Dependencies 

- [Cosmos DB Emulator](https://docs.microsoft.com/en-us/azure/cosmos-db/local-emulator#installation )
- [Node.js](https://nodejs.org/en/download/ )
- [Yarn](https://yarnpkg.com/en/)
  
