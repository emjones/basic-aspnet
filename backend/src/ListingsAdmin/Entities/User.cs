﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ListingsAdministration.Entities
{
    public class User
    {
        [Column("id")]
        public int Id { get; set; }
        [Column("first_name")]
        public string FirstName { get; set; }
        [Column("last_name")]
        public string LastName { get; set; }
        [Column("email")]
        public string Email { get; set; }
        [Column("is_agent")]
        public bool IsAgent { get; set; }
        [Column("password")]
        public string Password { get; set; }
        [Column("photo")]
        public string Photo { get; set; }

        //[Column("account_id")]
        //[ForeignKey("AccountId")]
        public Account Account { get; set; }
        [Column("account_id")]
        public int AccountId { get; set; }
    }
}
