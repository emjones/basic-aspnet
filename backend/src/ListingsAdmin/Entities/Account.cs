﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ListingsAdministration.Entities
{
    public class Account
    {
        //[Column("id")]
        public int Id { get; set; }
        //[Column("status")]
        public string Status { get; set; }
        public ICollection<User> Users { get; set; }
            = new List<User>();
    }
}
