﻿using ListingsAdministration.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ListingsAdministration
{
    public static class UserContextExtensions
    {
        public static void EnsureSeedDataForContext(this UserContext context)
        {
            if (context.Users.Any())
            {
                return;
            }
            var users = new List<User>()
            {
                new User()
                {
                        FirstName = "Em",
                        LastName = "J",
                        Email = "email@email",
                        IsAgent = false,
                        Password = "password",
                        Account = new Account()
                        {
                            Status = "active"
                        }
                },
                new User()
                {
                        FirstName = "Em",
                        LastName = "Y",
                        Email = "otherEmail@email",
                        IsAgent = false,
                        Password = "password",
                        Account = new Account()
                        {
                            Status = "active"
                        }
                }
            };
            context.Users.AddRange(users);
            context.SaveChanges();
        }
    }
}
