﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ListingsAdministration.Entities;
using ListingsAdministration.Models;
using ListingsAdministration.Services;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ListingsAdministration.Controllers
{
    [Route("api/account")]
    public class AccountController : Controller
    {
        private IAccountRepository _accountRepository;
        public AccountController(IAccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
        }
        // GET api/<controller>/5
        [HttpGet("{id}", Name="GetAccount")]
        public IActionResult GetAccount(int id)
        {
            var account = _accountRepository.GetAccount(id);
            if(account == null)
            {
                return NotFound();
            }
            var accountResult = Mapper.Map<AccountDto>(account);
            return Ok(accountResult);
        }

        // POST api/<controller>
        [HttpPost]
        public IActionResult CreateAccount(string status = "active")
        {
            var account = new AccountDto
            {
                Status = status
            };

            var accountToAdd = Mapper.Map<Entities.Account>(account);
            _accountRepository.CreateAccount(accountToAdd);
            if(!_accountRepository.Save())
            {
                return StatusCode(500, "A problem occurred while handling your request");
            }
            var accountToReturn = Mapper.Map<Models.AccountDto>(accountToAdd);
            return CreatedAtRoute("GetAccount", new { id = accountToReturn.Id }, accountToReturn);
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        [HttpPatch("{id}")]
        public IActionResult UpdateAccount(int id, [FromBody]JsonPatchDocument<AccountForUpdateDto> patchDocument)
        {
            if(patchDocument == null)
            {
                return BadRequest();
            }
            var accountEntity = _accountRepository.GetAccount(id);
            if(accountEntity == null)
            {
                return NotFound();
            }
            var accountToPatch = Mapper.Map<AccountForUpdateDto>(accountEntity);

            patchDocument.ApplyTo(accountToPatch, ModelState);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Mapper.Map(accountToPatch, accountEntity);

            if(!_accountRepository.Save())
            {
                return StatusCode(500, "A problem occurred while handling your request");
            }

            return NoContent();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public IActionResult DeactivateAccount(int id)
        {
            //needs account active bit before implemented
            //gonna patch
            return NoContent();
        }
        
        // DELETE api/<controller>/<action>/5
        [HttpDelete("[action]/{id}")]
        public IActionResult DeleteAccount(int id)
        {
            var accountEntity = _accountRepository.GetAccount(id);
            if (accountEntity == null)
            {
                return NotFound();
            }

            _accountRepository.DeleteAccount(accountEntity);

            if (!_accountRepository.Save())
            {
                return StatusCode(500, "A problem happened while handling your request");
            }

            return NoContent();
        }
    }
}
