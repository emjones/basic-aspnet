﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ListingsAdministration.Entities;
using ListingsAdministration.Models;
using ListingsAdministration.Services;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ListingsAdministration.Controllers
{
    [Route("api/users")]
    public class UsersController : Controller
    {
        private IUserRepository _userRepository;
        private IAccountRepository _accountRepository;

        public UsersController(IUserRepository userRepository, IAccountRepository accountRepository)
        {
            _userRepository = userRepository;
            _accountRepository = accountRepository;
        }

        [HttpGet()]
        public IActionResult GetUsers()
        {
            var userEntities = _userRepository.GetUsers();
            var results = Mapper.Map<IEnumerable<UserDto>>(userEntities);
            return Ok(results);
        }

        
        [HttpGet("{id}", Name = "GetUser")]
        public IActionResult GetUser(int id)
        {
            var user = _userRepository.GetUser(id);

            if(user == null)
            {
                    return NotFound();
            }

            var userResult = Mapper.Map<UserDto>(user);
            return Ok(userResult);
        }

        // POST api/<controller>
        [HttpPost]
        public IActionResult CreateUser([FromBody] UserDto user)
        {
            if(_userRepository.UserEmailExists(user.Email))
            {
                    return StatusCode(500, "A user with that email already exists.");
            }
            //is this a safe check? 0 is a valid pk
            if(int.Equals(user.Account.Id, default(int)))
            {
                //maybe sep function idk
                    var account = new AccountDto
                    {
                        Status = "new"
                    };
                    var accountToAdd = Mapper.Map<Entities.Account>(account);
                    _accountRepository.CreateAccount(accountToAdd);
                    if (!_accountRepository.Save())
                    {
                        return StatusCode(500, "A problem occurred while handling your request");
                    }
                    var accountForUser = Mapper.Map<Models.AccountDto>(accountToAdd);
                    user.Account = accountForUser;
            }
            var userToAdd = Mapper.Map<User>(user);
            int userId = _userRepository.CreateUser(userToAdd);
            //should both acctrepo and userrepo have a save? is same context but sorta confusing
            if(!_accountRepository.Save())
            {
                return StatusCode(500, "A problem occurred while handling your request");
            }
            var userToReturn = Mapper.Map<UserDto>(userToAdd);
            _accountRepository.AddUserToAccount(userToAdd.Account, userToAdd);
            return CreatedAtRoute("GetUser", new { id = userToReturn.Id }, userToReturn);
        }

        [HttpPatch("{id}")]
        public IActionResult UpdateUser(int id, [FromBody]JsonPatchDocument<UserForUpdateDto> patchDocument)
        {
            if (patchDocument == null)
            {
                return BadRequest();
            }
            var userEntity = _userRepository.GetUser(id);
            if (userEntity == null)
            {
                return NotFound();
            }
            var userToPatch = Mapper.Map<UserForUpdateDto>(userEntity);
            //var userToPatch = Mapper.Map<UserForUpdateDto>(userEntity);
            patchDocument.ApplyTo(userToPatch, ModelState);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Mapper.Map(userToPatch, userEntity);
            if (!_accountRepository.Save())
            {
                return StatusCode(500, "A problem occurred while handling your request");
            }
            return NoContent();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public IActionResult DeactivateUser(int id)
        {
            //needs account active bit before implemented
            //gonna patch
            return NoContent();
        }

        // DELETE api/<controller>/<action>/5
        [HttpDelete("[action]/{id}")]
        public IActionResult DeleteUser(int id)
        {
            var userEntity = _userRepository.GetUser(id);
            if (userEntity == null)
            {
                return NotFound();
            }

            _userRepository.DeleteUser(userEntity);

            if (!_accountRepository.Save())
            {
                return StatusCode(500, "A problem happened while handling your request");
            }

            return NoContent();
        }
    }
}
