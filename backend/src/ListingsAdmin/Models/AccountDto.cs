﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ListingsAdministration.Models
{
    public class AccountDto
    {
        public int Id { get; set; }
        public string Status { get; set; }
        public ICollection<UserDto> Users { get; set; }
            = new List<UserDto>();
    }
}
