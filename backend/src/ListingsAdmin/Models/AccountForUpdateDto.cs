﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ListingsAdministration.Models
{
    public class AccountForUpdateDto
    {
        public string Status { get; set; }
    }
}
