﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ListingsAdministration.Entities;
using ListingsAdministration.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace ListingsAdministration
{
    public class Startup
    {
        String MsSqlConnectionString;
        String NoSqlConnectionString;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            MsSqlConnectionString = configuration.GetConnectionString("MsSql");
            NoSqlConnectionString = configuration.GetConnectionString("NoSql");
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var mvc = services.AddMvc();
            mvc.AddJsonOptions(options => { options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore; });
            services.AddDbContext<UserContext>(o => o.UseSqlServer(MsSqlConnectionString));
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IAccountRepository, AccountRepository>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, UserContext userContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            userContext.EnsureSeedDataForContext();
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Entities.User, Models.UserDto>();
                cfg.CreateMap<Models.UserDto, Entities.User>();
                cfg.CreateMap<Models.UserForUpdateDto, Entities.User>();
                cfg.CreateMap<Entities.Account, Models.AccountDto>();
                cfg.CreateMap<Models.AccountDto, Entities.Account>();
                cfg.CreateMap<Models.AccountForUpdateDto, Entities.Account>();
                //cfg.CreateMap<Models.UserForUpdateDto, Entities.User>()
                //    .ForMember(x=>x.Id, y=>y.Ignore())
                //    .ForMember(x=>x.Account, y=>y.Ignore())
                //    .ForMember(x=>x.AccountId, y=>y.Ignore());
            });

            app.UseMvc();
        }

    }
}