﻿using ListingsAdministration.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ListingsAdministration.Services
{
    public class AccountRepository : IAccountRepository
    {
        private UserContext _context;
        public AccountRepository(UserContext context)
        {
            _context = context;
        }

        public bool AccountExists(int accountId)
        {
            return _context.Accounts.Any(c => c.Id == accountId);
        }

        public Account GetAccount(int accountId)
        {
            return _context.Accounts.Where(c => c.Id == accountId).FirstOrDefault();
        }

        public void CreateAccount(Account account)
        {
            _context.Add(account);
            //return account.Id;
        }
        public void AddUserToAccount(Account account, User user)
        {
            account.Users.Add(user);
        }
        public void DeleteAccount(Account account)
        {
            _context.Accounts.Remove(account);
        }
        public bool Save()
        {
            return (_context.SaveChanges() >= 0);
        }
    }
}
