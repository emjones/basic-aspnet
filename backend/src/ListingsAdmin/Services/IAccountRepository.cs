﻿using ListingsAdministration.Entities;

namespace ListingsAdministration.Services
{
    public interface IAccountRepository
    {
        bool AccountExists(int accountId);
        Account GetAccount(int accountId);
        void CreateAccount(Account account);
        void AddUserToAccount(Account account, User user);
        void DeleteAccount(Account account);
        bool Save();
    }
}