﻿using ListingsAdministration.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ListingsAdministration.Services
{
    public class UserRepository : IUserRepository
    {
        private UserContext _context;
        public UserRepository(UserContext context)
        {
            _context = context;
        }

        public bool UserExists(int userId)
        {
            return _context.Users.Any(c => c.Id == userId);
        }

        public bool UserEmailExists(string email)
        {
            return _context.Users.Any(c => c.Email == email);
        }

        public IEnumerable<User> GetUsers()
        {
            return _context.Users.Include(c=>c.Account).OrderBy(c => c.Id).ToList();
        }

        public User GetUser(int userId)
        {
            return _context.Users.Include(c=>c.Account).Where(c => c.Id == userId).FirstOrDefault();
        }

        public int CreateUser(User user)
        {
            _context.Add(user);
            return user.Id;
        }

        public void DeleteUser(User user)
        {
            _context.Users.Remove(user);
        }
    }
}
