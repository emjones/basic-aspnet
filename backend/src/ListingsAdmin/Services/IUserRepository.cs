﻿using ListingsAdministration.Entities;
using System.Collections.Generic;

namespace ListingsAdministration.Services
{
    public interface IUserRepository
    {
        bool UserExists(int userId);
        bool UserEmailExists(string email);
        IEnumerable<User> GetUsers();
        User GetUser(int userId);
        int CreateUser(User user);
        void DeleteUser(User user);
    }
}