# Backend

## VS Code

- Configure workspace: `File > Open Workspace...` -> Select "Backend.code-workspace"

## Code settings

- Configured using [these specifications](https://docs.microsoft.com/en-us/dotnet/core/tutorials/using-on-macos)
