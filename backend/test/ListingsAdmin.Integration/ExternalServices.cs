using System;
using System.Data.SqlClient;
using Xunit;

namespace Test.Integration
{
    /**
     * The ExternalServices class is intended to be used to test our configuration settings in various environments
     */
    public class ExternalServices
    {
        [Fact]
        public void CanConnectToEnvironmentDB()
        {
            //TODO: externalize connection string to config file
            var connection = new SqlConnection("Server=mssqls.anendeavor.com;Database=listings;User Id=sa;Password=Potat0Fong0;");
            connection.Open();
            Assert.NotEqual(System.Data.ConnectionState.Closed, connection.State);
        }
    }
}